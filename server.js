require('dotenv').config();
const express = require('express');
const path = require('path');
const cors = require('cors');
const bodyParser = require('body-parser');
const app = express();
const proxy = require('express-http-proxy');
const superagent = require('superagent');
const fs = require('fs');
const { parse } = require ('node-html-parser');
const setCookie = require('set-cookie-parser');
const compression = require('compression');
const spdy = require('spdy');
const request = require('request');

import config from './config';

const {
  server: { port: serverPort, host: serverHost },
  api: { graphql, search, api2, social, root },
  build: { dir },
  defaultSlug,
  cookieDomain
} = config;

app.use(compression({ filter: shouldCompress }))

function shouldCompress (req, res) {
  if (req.headers['x-no-compression']) {
    // don't compress responses with this request header
    return false
  }

  // fallback to standard filter function
  return compression.filter(req, res)
}

app.use(cors());

app.use((req, res, next) => {
  if (req.url === '/') {
    res.redirect(301, `/${defaultSlug}/home`)
  } else {
    next();
  }
});

app.use('/build', express.static(path.join(__dirname,  dir)));

const selectProxyHostGraphQL = (req) => {
  return `${graphql}/${req.params.slug}/graphql`
}
const selectProxyHostSearch = (req) => {
  return `${search}/debate/${req.params.slug}/_search`
}
const selectProxyDebate = (req) => {
  return `${api2}/data/Discussion/${req.params.debate}`
}
const selectProxySocial = (req) => {
  return `${social}/${req.params.slug}/login/${req.params.social}`
}

app.post(
  `/:slug/graphql`, proxy(selectProxyHostGraphQL)
);

app.post(
  '/:slug/_search',
  proxy(selectProxyHostSearch)
);

app.get(
  `/data/Discussion/:debate/*`, proxy(selectProxyDebate)
);
app.get(
  `/data/Discussion/:debate`, proxy(selectProxyDebate)
);

app.get(
  `/critical_information`, proxy(`${root}/critical_information`)
);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get(`/debate/:slug/logout`, async (req,res) => {
  const next = req.query.next;

  const response = await superagent.get(`${root}/debate/${req.params.slug}/logout`)
    .type('application/x-www-form-urlencoded')
    .send(req.body)

  if (response.statusCode === 200) {
    res.clearCookie("assembl_session", { domain: cookieDomain, path: '/', secure: true, httpOnly: true });
    res.redirect(next);
  }
});

app.post(`/debate/:slug/login`, async (req,res) => {
  const next = req.body.next;

  const options = {
    'method': 'POST',
    'url': `${root}/debate/${req.params.slug}/login`,
    'headers': {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Cookie': req.headers.cookie
    },
    form: {
      'password': req.body.password,
      'identifier': req.body.identifier
    }
  };

  request(options, (error, response) => {
    if (error) throw new Error(error);

    if (response.statusCode === 401) {
      // Not an ok response
      res.statusCode = response.statusCode;

      // Santise the response
      const message = response.toJSON()
      message.body = message.body.replace('401 Unauthorized\n' +
      '\n' +
      'This server could not verify that you are authorized to access the document you requested.  Either you supplied the wrong credentials (e.g., bad password), or your browser does not understand how to supply the credentials required.\n' +
      '\n' +
      '\n', '');

      return res.send(message);
    }

    // We should parse out the 'data' supplied from the html response for a JWT token the front can use
    // Then redirect to the page the user came from
    const cookiesAssembl = setCookie.parse(response);

    cookiesAssembl.map((wpa) => {
      res.cookie(wpa.name, wpa.value, { domain: cookieDomain, path: '/', secure: true, httpOnly: true });
    })

    return res.redirect(next);
  });
});

app.get(`/:slug/login/:social`, proxy(selectProxySocial));

app.get(`/:slug/*`, (req, res) => {
  res.sendFile(path.join(__dirname + '/build/index.html'));
});

app.post('/data/AgentProfile/password_reset', proxy(`${api2}/data/AgentProfile/password_reset`));

app.post('/data/AgentProfile/do_password_change', proxy(`${api2}/data/AgentProfile/do_password_change`));

app.post(
  `/data/Discussion/:debate/*`, proxy(selectProxyDebate)
);
app.post(
  `/data/Discussion/:debate`, proxy(selectProxyDebate)
);

app.get(`/*`, (req, res) => {
  res.sendFile(path.join(__dirname + '/build/index.html'));
});


if (process.env.NODE_ENV === 'development') {
  spdy
    .createServer(
      {
        key: fs.readFileSync('./cert/server.key'),
        cert: fs.readFileSync('./cert/server.crt')
      },
      app
    )
    .listen(serverPort);
} else {
  spdy
    .createServer({}, app).listen(serverPort);
}
console.log (`server listening on ${serverPort}`);
