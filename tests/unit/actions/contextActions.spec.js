import * as actions from '../../../src/js/actions/contextActions';

describe('Context actions', () => {
  describe('toggleHarvesting action', () => {
    const { toggleHarvesting } = actions;
    it('should return a TOGGLE_HARVESTING action type', () => {
      const expected = {
        type: 'TOGGLE_HARVESTING'
      };
      const actual = toggleHarvesting();
      expect(actual).toEqual(expected);
    });
  });

  describe('assemblVersion action', () => {
    const { fetchAssemblVersion } = actions;
    it('should call an action with assemblVersion data', () => {
      const data = {'assembl_version': 'xxx'}
      const expected = {
        type: 'FETCH_ASSEMBL_VERSION',
        assemblVersion: 'xxx'
      }
      const actual = fetchAssemblVersion(data);
      expect(actual).toEqual(expected);
    })
  })
});