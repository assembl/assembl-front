import React, { Suspense } from 'react';
import { create } from 'react-test-renderer';

import { DumbMenuItem } from '../../../../../src/js/components/debate/navigation/menuItem';

describe('DumbMenuItem component', () => {
  it('should match a selected menu item', async () => {
    const props = {
      item: {
        id: 'fooId',
        title: 'Foo',
        img: {
          externalUrl: 'https://foo.bar/img'
        },
        numContributors: 10,
        numPosts: 123,
        numVotes: 0,
        messageViewOverride: 'thread'
      },
      identifier: 'survey',
      selected: true,
      hasSubItems: true,
      slug: 'slug'
    };

    const root = create(
      <Suspense fallback={<div>loading...</div>}>
        <DumbMenuItem {...props} />
      </Suspense>
    );
    await new Promise(resolve => setTimeout(resolve, 0));

    expect(root).toMatchSnapshot();
  });

  it('should match a not selected menu item', async () => {
    const props = {
      item: {
        id: 'fooId',
        title: 'Foo',
        img: {
          externalUrl: 'https://foo.bar/img'
        },
        numContributors: 10,
        numPosts: 123,
        numVotes: 0,
        messageViewOverride: 'thread'
      },
      identifier: 'survey',
      selected: false,
      hasSubItems: true,
      slug: 'slug'
    };
    const root = create(
      <Suspense fallback={<div>loading...</div>}>
        <DumbMenuItem {...props} />
      </Suspense>
    );
    await new Promise(resolve => setTimeout(resolve, 0));

    expect(root).toMatchSnapshot();
  });

  it('should match a selected menu item of type vote session', async () => {
    const props = {
      item: {
        id: 'fooId',
        title: 'Foo',
        img: {
          externalUrl: 'https://foo.bar/img'
        },
        numContributors: 10,
        numPosts: 0,
        numVotes: 123,
        messageViewOverride: 'voteSession'
      },
      identifier: 'survey',
      selected: true,
      hasSubItems: true,
      slug: 'slug'
    };
    const root = create(
      <Suspense fallback={<div>loading...</div>}>
        <DumbMenuItem {...props} />
      </Suspense>
    );
    await new Promise(resolve => setTimeout(resolve, 0));

    expect(root).toMatchSnapshot();
  });
});