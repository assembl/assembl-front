import React from 'react';
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';
import { MockedProvider } from 'react-apollo/test-utils';

import AssemblLogin from '../../../../src/js/components/login/assemblLogin';

const mockStore = configureStore();

const initialState = {
  auth: {
    signupSuccess: {
      success: false,
      reason: null
    },
    signinSuccess: {
      success: false,
      reason: null
    },
    passwordChangeRequest: {
      success: null
    }
  }
};

describe('AssemblLogin component', () => {
  it('should match the snapshot', () => {
    const store = mockStore(initialState);
    const component = renderer.create(
      <MockedProvider store={store}>
        <AssemblLogin />
      </MockedProvider>
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});