import ContextReducer from "../../../src/js/reducers/contextReducer";

describe("Return context state changes", () => {
  it("Should handle ADD_CONTEXT", () => {
    expect(
      ContextReducer([], {
        type: "ADD_CONTEXT",
        rootPath: undefined,
        debateId: "7",
      })
    ).toEqual({
      rootPath: undefined,
      debateId: "7",
    });
  });

  it("Should handle FETCH_ASSEMBL_VERSION", () => {
    expect(
      ContextReducer([], {
        type: "FETCH_ASSEMBL_VERSION",
        assemblVersion: "xxx",
      })
    ).toEqual({
      assemblVersion: "xxx",
    });
  });
});
