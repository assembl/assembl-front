FROM node:12.18-alpine

# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install app dependencies
COPY package.json yarn.lock ./

# Make sure git is available to image
RUN apk add --no-cache git

RUN yarn install

# add app
COPY . ./

#RUN ls /app/node_modules/assembl-editor-utils

# build it
RUN yarn run build
