[![Quality Gate Status](https://sonarcube.bluenove.com/api/project_badges/measure?project=assembl-front%3Amaster&metric=alert_status)](https://sonarcube.bluenove.com/dashboard?id=assembl-front%3Amaster)
# Overview

The assembl front (built with react) seperated from the assembl back (as an api)

### Why

Well the traditional way to build react apps is to make them independant from anything else. In assembl land they just added a new directory, shoved the react in there and continued to grow the monolith which in turn makes it harder to scale (only horizontally with more assembls behind a load balancer).

The reasoning for this approach was that the index.html is generated on the fly from a jinja2 template by the assembl pyramid http server and config params are embedded into this html to tell the front end about stuff it doesn't know like the user-name. This in itself is a shitty way to do things - the front should always query the back for this type of information via an API.

The 2nd problem is that translations are maintained on the back, exported and then imported into the front. I suspect to reduce the amount of translations and maintenance necessary but again, traditionally front-end translations should not have a dependence on the back - the back should return an error, the front chooses how to display according to language but of course assembl does the translation on the server as well - it's messy and it's now bit broken with this separation. Translations will have to be exported, and committed to this repo which is more overhead but is cleaner in the long run.

So, by taking the front out, the assembl server itself will have to manage less work in serving the react part as that is handled separately (and dockerisable etc see later), which means a better scaling pattern also, any front end developer does not need to install the whole python environment and try to understand what's going on. The can simply, fire up this repo and point it at a UAT / Sandbox or prod platform as long as they can login to it.

All requests go from the react front runniing on a individuals computer back to the server.js which is serving this on the server to be proxed through to the assembl back. This avoids CORS problems and the like.

See the TODO list for current status of events

### Requirements

Install Node 12.18.3

For nvm users, just move to the project directory and run :
```
    nvm i
```
If you already have installed Node 12.18.3 before, just type:
```
    nvm use
```

### Install

YOU HAVE TO USE YARN, yes I repeat yarn for installing the deps - if you use npm, it will work but will not run. There's some deep nested crap in here, mostly related to the workspaces which should NOT be present in this repo (see TODO). These should be exteral repos that build and push to npmjs and then just included here like any other package.

## Run it

First you have to start the webpack-dev-server. It outputs into ./build/ which is watched by our proxy server. 
Moreover, you will benefit from its hot-reload module, which is very convenient while developing.

1. yarn start

To run the proxy server, you need first to run:
2. yarn start:dev
=> As spdy is used to create the webserver while using HTTP2, you app will be:

https://localhost:3000  

## Tests

The tests are now runing which means they should be run on a lovall development machine before committing. They will be run as part of the build pipeline on gitlab anyway so this will fail a build.

## Config

In the main assembl-front works out a lot of stuff for itself, it uses the slug to query the server and find the debate id and then using the debate id it goes and gets preferences etc. So an assembl-front will work in a saas capacity in front of a single assembl (that might have multiple debates)

Variable name | Description  | Default
------------- | ------------ | -------------
API_GRAPHQL | The url to the graphql endpoint | 'https://sandbox-assembl.cloud.bluenove.com'
API_SEARCH | The url to the search endpoint | 'https://sandbox-assembl.cloud.bluenove.com'
API_API2 | The url to the api2(rest) endpoint | 'https://sandbox-assembl.cloud.bluenove.com'
API_SOCIAL | The url to the social login endpoint | 'https://sandbox-assembl.cloud.bluenove.com'
BUILD_DIR | Where to find the built react code| 'build'
COOKIE_DOMAIN | Domain where the session cookie should be stored | 'localhost'
DEFAULT_SLUG | If no slug provided, this is the default | ''
SERVER_HOST | The name of the server (normally can be left alone) | ''
PORT | The port to run  on | 3000

# DEPLOY
010

# TODO

- [ ] Better eslint rules
- [ ] Prune 'run' jobs in the package.json
- [ ] Re-organise code into better structure
- [ ] Convert classes to pure functions with hooks
- [x] Get tests working and useful
- [ ] Bring up to date all packages
- [x] Dockerise
- [x] Remove workspaces to be just components (don't depend on yarn)
- [x] Improve proxy in server so that any slug can be used
- [x] Build api in back end so that variables are not stored in <input hidden> fields in the index.html
- [x] Build git flow
- [ ] Workout how to update translations from the back to front that rely on .po files
