import Cookies from 'js-cookie';

export const getCookieItem = (sKey: string) => Cookies.get(sKey);

export function setCookieItem(name: string, value: any, sensitive: boolean = false) {
  const date = new Date();
  let finalValue = null;

  date.setMonth(date.getMonth() + 13);
  const extras: Object = { path: '/', expires: date };
  if (sensitive) {
    extras.secure = true;
  }
  if (Array.isArray(value)) {
    finalValue = value.join(',');
  } else {
    finalValue = value;
  }
  Cookies.set(name, finalValue, extras);
}