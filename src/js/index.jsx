import 'core-js';
import 'regenerator-runtime/runtime';
import 'custom-event-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import { ApolloProvider } from 'react-apollo';
import { Router } from 'react-router';

import client from './client';
import Routes from './routes';
import hashLinkScroll from './utils/hashLinkScroll';
import { initializeSentry } from './utils/sentry';
import { ScreenDimensionsProvider } from './utils/screenDimensions';
import { browserHistory } from './router';
import GlobalErrorBoundary from './components/common/GlobalErrorBoundary';
import store from './store';

require('smoothscroll-polyfill').polyfill();

const rootElement = document.getElementById('root');
initializeSentry(rootElement);

const renderAssembl = (routes) => {
  ReactDOM.render(
    <ApolloProvider store={store} client={client}>
      <ScreenDimensionsProvider>
        <GlobalErrorBoundary>
          <Router
            history={browserHistory}
            routes={routes}
            onUpdate={() => {
              window.scrollTo({ top: 0, left: 0, behavior: 'smooth' });
              hashLinkScroll();
            }}
          />
        </GlobalErrorBoundary>
      </ScreenDimensionsProvider>
    </ApolloProvider>,
    rootElement
  );
};

renderAssembl(Routes);
