# Overview

The js directory contains everything unsurprisingly which is js and jsx. It's pretty messy in here and needs a lot of work, see the checklist.

### Requirements

Install Node 12.13.1

For nvm users, just move to the project directory and run :
```
    nvm i
```
If you already have installed Node 10.16.0 before, just type:
```
    nvm use
```
## Run it

There is no specific hot-reload webpack dev config - this is because the front always needs to pass via a proxy and this proxy is based upon config e.g. I am the front for this slug and debate. Ultimatley we can remove this to be multi-purose (see checklist) but this will be difficult to build into a webpack proxy in any case. So start:dev uses the re-build stuff from npm run build (written to the build directory) as does start:prod but start:prod does not have nodemon for checking for js file changes and reloading.

SO, if you change jsx, you  WILL need to run build!

```
npm run start:dev
or
npm run start:prod
```

# TODO

- [x] Remove useless app sub-directory
- [ ] Re-order imports for readability
- [ ] Better eslint rules
- [ ] Get tests working and useful
