// @flow

export const setDiscussionData = (discussionPreferences: object) => ({
  discussionId: discussionPreferences.parentId,
  discussionPreferences,
  type: 'SET_DISCUSSION_DATA'
});