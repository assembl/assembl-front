import { resolvedFetchUserData, failedFetchUserData, loadingUserData } from './userActions'
import { getCriticalInfo } from "../services/criticalInfoService";

const resolvedAddContext = (rootPath, debateId) => ({
  type: "ADD_CONTEXT",
  rootPath: rootPath,
  debateId: debateId,
});

export const addContext = (rootPath, debateId) =>
  function(dispatch) {
    dispatch(resolvedAddContext(rootPath, debateId));
  };

export const fetchAssemblVersion = (data) => {
  return ({
    type: "FETCH_ASSEMBL_VERSION",
    assemblVersion: data["assembl_version"] ?? null,
  })
};

export const fetchInitStateApp = (discussionId) =>
  function(dispatch) {
    dispatch(loadingUserData());
    return getCriticalInfo(discussionId).then(
      (data) => {
        dispatch(resolvedFetchUserData(data));
        dispatch(fetchAssemblVersion(data));
      },
      (error) => {
        dispatch(failedFetchUserData(error));
      }
    );
  };

export const toggleHarvesting = () => ({ type: "TOGGLE_HARVESTING" });
