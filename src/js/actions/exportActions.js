import { exportFirstVisit, exportLastVisit } from '../services/exportService';

const exportDone = () => ({
  type: 'EXPORT_DONE'
});

const exporVisitData = () => ({
  type: 'EXPORT_VISIT_DATA'
});

const resolvedExportVisitData = visitData => ({
  type: 'RESOLVED_EXPORT_VISIT_DATA',
  visitData
});

const failedExportVisitData = error => ({
  type: 'FAILED_EXPORT_VISIT_DATA',
  exportError: error
});

export const exportFirstVisitRequest = (discussionId) =>
  function (dispatch) {
    dispatch(exporVisitData());
    return exportFirstVisit(discussionId).then(
      (userData) => {
        dispatch(resolvedExportVisitData(userData));
      },
      (error) => {
        dispatch(failedExportVisitData(error));
      }
    );
  };

  export const exportLastVisitRequest = (discussionId) =>
  function (dispatch) {
    dispatch(exporVisitData());
    return exportLastVisit(discussionId).then(
      (userData) => {
        dispatch(resolvedExportVisitData(userData));
      },
      (error) => {
        dispatch(failedExportVisitData(error));
      }
    );
  };

  export const exportDoneRequest = () =>
  function (dispatch) {
    dispatch(exportDone());
  };