export const loadingUserData = () => ({
  type: "FETCH_USER_DATA",
  debateData: null,
});

export const resolvedFetchUserData = (userData) => ({
  type: "RESOLVED_FETCH_USER_DATA",
  userData,
});

export const failedFetchUserData = (error) => ({
  type: "FAILED_FETCH_USER_DATA",
  userError: error,
});
