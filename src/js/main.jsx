// @flow
import * as React from 'react';
import { connect } from 'react-redux';

import { getCurrentPhaseData, getPhaseId } from './utils/timeline';
import AssemblNavbar from './components/navbar/navbar';
import Footer from './components/common/footer';
import CookiesBar from './components/cookiesBar';
import AcceptCookiesModal from './components/cookies/acceptCookiesModal';
import { fromGlobalId } from './utils/globalFunctions';
import { MatomoProvider, createInstance } from '@datapunt/matomo-tracker-react'
import { DEFAULT_FAVICON } from './constants';
import { Helmet } from 'react-helmet';
import { getDiscussionSlug } from './utils/globalFunctions';
import { browserHistory } from './router';

type Props = {
  timeline: Timeline,
  params: { phase?: string, themeId?: string },
  location: { pathname: string },
  children: React.Node
};

class Main extends React.Component<Props> {
  render() {
    const { routeParams, params, timeline, location, locale, user: { userData }, discussion: { matomoID, favicon, tab_title, extra_json, privateDebate }, debateData } = this.props;
    const { themeId } = params;
    const { currentPhaseIdentifier, currentPhaseId } = getCurrentPhaseData(timeline);
    let identifier = params.phase || null;
    let phaseId = currentPhaseId;
    if (!identifier) {
      identifier = currentPhaseIdentifier;
    } else {
      phaseId = getPhaseId(timeline, identifier);
    }
    const discussionPhaseId = phaseId ? fromGlobalId(phaseId) : null;
    const children = React.Children.map(this.props.children, child =>
      React.cloneElement(child, {
        identifier: identifier,
        phaseId: phaseId,
        discussionPhaseId: discussionPhaseId
      })
    );

    const instanceMatomo = createInstance({
      urlBase: 'https://bluenove.matomo.cloud',
      siteId: matomoID,
      disabled: false,
      linkTracking: true,
      srcUrl: '//cdn.matomo.cloud/bluenove.matomo.cloud/matomo.js',
      trackerUrl: '//bluenove.matomo.cloud/matomo.php'
    })

    const faviconUrl = favicon ? favicon.externalUrl : DEFAULT_FAVICON;

    // Open graph and link texts
    let topicText = 'No topic available in the extra json';
    if (extra_json && extra_json.topic && extra_json.topic.titleEntries) {
      topicText = topicText = extra_json.topic.titleEntries[locale]
    }

    if (privateDebate && userData && !userData.id) {
      // Redirect to login
      const slug = getDiscussionSlug();
      browserHistory.push(`/${slug}/login?next=/${slug}/home`);
    }

    // Is this debate private, if so and the user is not authenticated - redirect to login
    return (
      <>
      <Helmet>
        <title>{tab_title}</title>
        <meta name="og:title" content={topicText}></meta>
        <meta property="og:type" content="website"></meta>
        <meta property="og:url" content={`${window.location.protocol}/${window.location.hostname}/${getDiscussionSlug()}`}></meta>
        <meta property="og:image" content={debateData.headerPageImage}></meta>
        <meta property="og:image:width" content="1600"></meta>
        <meta property="og:image:height" content="222"></meta>
        <meta property="og:locale" content="fr"></meta>
        <meta name="twitter:card" content="summary_large_image"></meta>
        <meta name="twitter:site" content=""></meta>
        <meta name="twitter:creator" content="@bluenove"></meta>
        <meta name="twitter:title" content={topicText}></meta>
        <meta name="twitter:image" content={debateData.headerPageImage}></meta> 
        {privateDebate ? (<meta name="robots" content="noindex"></meta>) : null}                                                              
        <link rel="icon" href={`${faviconUrl}`} type="image/x-icon" />
      </Helmet>
      <MatomoProvider value={instanceMatomo}>
        <div className="main">
          <AssemblNavbar location={location.pathname} themeId={themeId} />
          <div className="app-content">{children}</div>
          <AcceptCookiesModal pathname={location.pathname} />
          <CookiesBar />
          <Footer />
        </div>
      </MatomoProvider>
      </>
    );
  }
}

const mapStateToProps = (state: { timeline: Timeline }) => ({
  timeline: state.timeline,
  discussion: state.discussion,
  locale: state.i18n.locale,
  debateData: state.debate.debateData,
  user: state.user
});

export default connect(mapStateToProps)(Main);