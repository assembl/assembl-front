const initialState = {
  discussionId: '',
  matomoID: null,
  languages: [],
  providers: [],
  tab_title: null,
  favicon: null,
  extra_json: null,
  privateDebate: false,
  useExternalHomepage: false,
  externalHomepageUrl: '',
  useCaptcha: false,
  captchaKey: ''
};

const DiscussionReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_DISCUSSION_DATA':
      const localProviders = [];
      if (action.discussionPreferences.facebookActivated) {
        localProviders.push({
          name: "Facebook",
        })
      }
      if (action.discussionPreferences.twitterActivated) {
        localProviders.push({
          name: "Twitter",
        })
      }
      if (action.discussionPreferences.instagramActivated) {
        localProviders.push({
          name: "Instagram",
        })
      }
      if (action.discussionPreferences.linkedinActivated) {
        localProviders.push({
          name: "LinkedIn",
        })
      }
      if (action.discussionPreferences.googleActivated) {
        localProviders.push({
          name: "google-oauth2",
        })
      }

      return {
        ...state,
        discussionId: action.discussionId,
        languages: action.discussionPreferences.languages,
        providers: localProviders,
        tab_title: action.discussionPreferences.tabTitle,
        favicon: action.discussionPreferences.favicon,
        extra_json: JSON.parse(action.discussionPreferences.extraJson),
        matomoID: action.discussionPreferences.webAnalyticsPiwikIdSite ? action.discussionPreferences.webAnalyticsPiwikIdSite : null,
        privateDebate: action.discussionPreferences.privateDebate ? action.discussionPreferences.privateDebate : false,
        useExternalHomepage: action.discussionPreferences.useExternalHomepage ? action.discussionPreferences.useExternalHomepage : false,
        externalHomepageUrl: action.discussionPreferences.externalHomepageUrl ? action.discussionPreferences.externalHomepageUrl : '',
        useCaptcha: action.discussionPreferences.useCaptcha ? action.discussionPreferences.useCaptcha : false,
        captchaKey: action.discussionPreferences.captchaKey ? action.discussionPreferences.captchaKey : ''
      };
    default:
      return state;
  }
};

export default DiscussionReducer;