const initialState = {
  visitData: [],
  exportRunning: false,
  exportError: null
};

const ExportReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'EXPORT_VISIT_DATA':
      return {visitData: [], exportRunning: true, exportError: null };
    case 'RESOLVED_EXPORT_VISIT_DATA':
      // Convert to an array for exporting
      const localVisitData = action.visitData.split('\n')
      const final = [];
      const moreRows = localVisitData.map((row) => {
         final.push(row.split(';'))
      })
      return { visitData: final, exportRunning: false, exportError: null };
    case 'FAILED_EXPORT_VISIT_DATA':
      return { visitData: [], exportRunning: false, exportError: action.exportError };
    case 'EXPORT_DONE':
      return { visitData: [], exportRunning: false, exportError: null };
    default:
    return state;
  }
};

export default ExportReducer;