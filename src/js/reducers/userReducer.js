const UserReducer = (state = {}, action) => {
  switch (action.type) {
    case 'FETCH_USER_DATA':
      return {userData: null, userLoading: true, userError: null };
    case 'RESOLVED_FETCH_USER_DATA':
      return { userData: action.userData, userLoading: false, userError: null };
    case 'FAILED_FETCH_USER_DATA':
      return { userData: null, userLoading: false, userError: action.userError };
    default:
      return state;
  }
};

export default UserReducer;