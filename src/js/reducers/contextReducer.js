const assemblVersionNode = document.getElementById('assemblVersion');
const assemblVersion = assemblVersionNode ? assemblVersionNode.value : null;
const initialState = {
  assemblVersion: assemblVersion,
  rootPath: null,
  debateId: null,
  isHarvesting: false
};

const ContextReducer = (state = initialState, action) => {
  switch (action.type) {
  case 'ADD_CONTEXT':
    return {
      ...state,
      rootPath: action.rootPath,
      debateId: action.debateId
    };
  case 'FETCH_ASSEMBL_VERSION':
    return {
      ...state,
      assemblVersion: action.assemblVersion
    };
  case 'TOGGLE_HARVESTING':
    return {
      ...state,
      isHarvesting: !state.isHarvesting
    };
  default:
    return state;
  }
};

export default ContextReducer;

export const getDebateId = state => state.context.debateId;

export const getLocale = state => state.i18n.locale;