// @flow
/*
  Text field adapter for react-final-form
*/
import React from 'react';
import { type FieldRenderProps } from 'react-final-form';
import { ControlLabel, FormGroup, FormControl } from 'react-bootstrap';

import Error from './error';
import { getValidationState } from './utils';

type Props = {
  input: {
    name: string,
    onBlur: (?SyntheticFocusEvent<*>) => void,
    onChange: (SyntheticInputEvent<*> | any) => void,
    onFocus: (?SyntheticFocusEvent<*>) => void,
    value: string
  },
  required: boolean,
  label: string,
  as: ?string
} & FieldRenderProps;

const TextFieldAdapter = ({
  input: { name, onChange, value, ...otherListeners },
  label,
  meta: { error, touched },
  required,
  as,
  ...rest
}: Props) => (
  <FormGroup controlId={name} as={as} validationState={getValidationState(error, touched)}>
    {value ? <ControlLabel>{required ? `${label} *` : label}</ControlLabel> : null}
    {as ? (
      <FormControl
        {...otherListeners}
        {...rest}
        componentClass="textarea"
        onChange={event => onChange(event.target.value)}
        placeholder={label}
        value={value || ''}
      />
    ): (
    <FormControl
      {...otherListeners}
      {...rest}
      onChange={event => onChange(event.target.value)}
      placeholder={label}
      value={value || ''}
    />)}
    <Error name={name} />
  </FormGroup>
);

export default TextFieldAdapter;