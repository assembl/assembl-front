// @flow
import React, { useState, useEffect } from 'react';
import HCaptcha from '@hcaptcha/react-hcaptcha';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { form, FormGroup, FormControl, Button } from 'react-bootstrap';
import { Translate, I18n } from 'react-redux-i18n';
import { get, getFullPath, getContextual } from '../../utils/routeMap';
import { getDiscussionSlug, useCaptcha, getCaptchaSiteKey } from '../../utils/globalFunctions';
import { displayAlert } from '../../utils/utilityManager';
import { signinAction } from '../../actions/authenticationActions';

type Props = {
  next: ?string,
  slug: ?string
};

const AssemblLogin = (props: Props) => {
  const [token, setToken: Function] = useState(null);
  const [identifier, setIdentifier] = useState('');
  const [password, setPassword] = useState('');
  const { slug } = props;
  const defaultNext = slug ? get('home', { slug: slug }) : null;
  const next = props.next || defaultNext;
  const login = slug ? getFullPath('ctxOldLogin', { slug: slug }) : getFullPath('oldLogin');
  const requestPasswordChange = slug ? getContextual('requestPasswordChange', { slug: slug }) : get('requestPasswordChange');
  const signUp = slug ? getContextual('signup', { slug: slug }) : get('signup');
  const showCaptcha = useCaptcha();

  const { auth } = props;

  const handleVerificationSuccess = (token) => {
    if (token) {
      setToken(token);
    }
  };

  const isDisabled = () => {
    if (showCaptcha) { return !token; }
    return false;
  };

  const signinHandler = (e, props) => {
    const { signIn } = props;
    const slug = getDiscussionSlug();

    e.preventDefault();
    signIn({
      next: `/${slug}/home`,
      referrer: 'v2',
      identifier,
      password,
      'captcha-response-token': token
    }, slug);
  };

  useEffect(() => {
    if (auth.signinSuccess && auth.signinSuccess.data) {
      displayAlert('danger', auth.signinSuccess.data.body)
    }
    if (auth.signinSuccess && auth.signinSuccess.success) {
      window.location.href = next;
    }
  }, [auth.signinSuccess]);

  return (
    <div>
      <form className="login" method="POST" onSubmit={(e) => {
        signinHandler(e, props)
      }}>
        {next ? <input type="hidden" name="next" value={next} /> : null}
        <h4 className="dark-title-4">
          <Translate dangerousHTML value="login.alreadyAccount" />
        </h4>
        <FormGroup className="margin-m">
          <FormControl 
            type="text" 
            name="identifier" 
            required 
            placeholder={I18n.t('login.username')} 
            value={identifier}
            onChange={(e) => {
              setIdentifier(e.target.value)
            }}
          />
        </FormGroup>
        <FormGroup>
          <FormControl
            type="password"
            autoComplete="new-password"
            name="password"
            required
            value={password}
            onChange={(e) => {
              setPassword(e.target.value)
            }}
            placeholder={I18n.t('login.password')}
          />
        </FormGroup>
        {showCaptcha ? <HCaptcha sitekey={getCaptchaSiteKey()} onVerify={token => handleVerificationSuccess(token)} /> : null}
        <FormGroup>
          <Button
            type="submit"
            name="login"
            value={I18n.t('login.login')}
            className="button-submit button-dark"
            disabled={isDisabled()}
          >
            <Translate value="login.login" />
          </Button>
        </FormGroup>
        {slug ? (
          <Link to={requestPasswordChange}>
            <Translate value="login.forgotPwd" />
          </Link>
        ) : null}
      </form>
      {slug ? (
        <div className="signup border-top margin-m">
          <h4 className="dark-title-4 margin-m">
            <Translate dangerousHTML value="login.noAccount" />
          </h4>
          <Link className="button-link button-dark margin-s" to={signUp}>
            <Translate value="login.signUp" />
          </Link>
        </div>
      ) : null}
    </div>
  );
};
const mapStateToProps = state => ({
  auth: state.auth
});

const mapDispatchToProps = dispatch => ({
  signIn: (payload, slug) => {
    dispatch(signinAction(payload, slug));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(AssemblLogin);
