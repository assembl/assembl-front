// @flow
import type { ApolloClient } from 'react-apollo';

import { getDiscussionID } from '../../../utils/globalFunctions';
import AgentProfilesQuery from '../../../graphql/AgentProfilesAdmin.graphql';
import AgentProfilesQueryWithEmail from '../../../graphql/AgentProfilesAdminWithEmail.graphql';

export const load = async (client: ApolloClient, fetchPolicy: FetchPolicy, offset: Int, limit: Int) => {
  const { data: agentProfiles } = await client.query({
    query: AgentProfilesQuery,
    fetchPolicy: fetchPolicy,
    variables: { offset, limit }
  });

  return agentProfiles;
};

export const lazyLoad = async (client: ApolloClient, fetchPolicy: FetchPolicy, email: String, offset: Int, limit: Int) => {
  const { data: agentProfiles } = await client.query({
    query: AgentProfilesQueryWithEmail,
    fetchPolicy: fetchPolicy,
    variables: { email: `%${email}%`, offset, limit }
  });

  return agentProfiles;
};

const findRoleAdmin = (role) => {
  if (role.role.name === 'r:administrator') {
    return true;
  } else {
    return false;
  }
}

const findRoleCatcher = (role) => {
  if (role.role.name === 'r:catcher') {
    return true;
  } else {
    return false;
  }
}

const findRoleModerator = (role) => {
  if (role.role.name === 'r:moderator') {
    return true;
  } else {
    return false;
  }
}

const findRoleParticipant = (role) => {
  if (role.role.name === 'r:participant') {
    return true;
  } else {
    return false;
  }
}

const findRoleSysadmin = (role) => {
  if (role.role.name === 'r:sysadmin') {
    return true;
  } else {
    return false;
  }
}

export function postLoadFormat(data) {
  const discussionID = getDiscussionID();

  // Loop through all roles and create a grid (excluding roles not for this debate)
  if (data.agentProfiles && data.agentProfiles.length > 0) {
    const values = data.agentProfiles.map((profile, idx) => {
      const rolesToKeep = []

      if (profile.localRoles && profile.localRoles.length > 0) {
        profile.localRoles.map((role) => {
          if (role.discussionId === discussionID) {
             return rolesToKeep.push(role)
          }
        })
      }
      // These are the roles I'm keeping for this debate so create a grid for the form
      const values = {};
      if (rolesToKeep.some(findRoleAdmin)) {
        values[`${idx}-r:administrator`] = true
      } else {
        values[`${idx}-r:administrator`] = false
      }         

      if (rolesToKeep.find(findRoleCatcher)) {
        values[`${idx}-r:catcher`] = true
      } else {
        values[`${idx}-r:catcher`] = false
      }         

      if (rolesToKeep.find(findRoleModerator)) {
        values[`${idx}-r:moderator`] = true
      } else {
        values[`${idx}-r:moderator`] = false
      }         

      if (rolesToKeep.find(findRoleParticipant)) {
        values[`${idx}-r:participant`] = true
      } else {
        values[`${idx}-r:participant`] = false
      }         

      if (rolesToKeep.find(findRoleSysadmin)) {
        values[`${idx}-r:sysadmin`] = true
      } else {
        values[`${idx}-r:sysadmin`] = false
      }         

      profile.localRoles = rolesToKeep;

      return values;
    })

    const returnVals = {
      more: data.more,
      adaptedProfiles: data.agentProfiles,
      offset: 0,
      limit: 5
    }

    values.map((value, idx) => {
      Object.keys(value).map((item, idx2) => {
        returnVals[item] = value[item]
        return null;
      })

      return null;
    })

    return returnVals;
  }
}