// @flow
import type { ApolloClient } from 'react-apollo';
import { isEqual } from 'lodash';

import updateAgentProfileRole from '../../../graphql/mutations/updateAgentProfileRole.graphql';
import { createSave } from '../../form/utils';

const getVariables = (values, initialValues) => {
  // Work out what's changed
  const toChange = Object.keys(values);
  const toChangeLength = toChange.length;
  const changed = {};

  for (let i = 0; i <= toChangeLength; i += 1) {
    if (!isEqual(values[toChange[i]], initialValues[toChange[i]])) {
      changed[toChange[i]] = values[toChange[i]];
    }
  }

  const variables = Object.keys(changed).map((value) => {
    const sep = value.split('-')

    return {
      id: values.adaptedProfiles[parseInt(sep[0], 10)].id,
      role: sep[1],
      action: changed[value] === true ? 'add' : 'remove'
    }
  })

  return variables;
}

export const createMutationsPromises = (client) => (
  values,
  initialValues
) => {
    const variables = getVariables(values, initialValues);

    const mutations = variables.map((variable) => {
      return () => client.mutate({
        mutation: updateAgentProfileRole,
        variables: variable
      });
    })

    return mutations;
};

export const save = createSave('administration.permissions.successSave');