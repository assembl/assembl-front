// @flow
import React from 'react';
import isEqualWith from 'lodash/isEqualWith';
import { connect } from 'react-redux';
import { I18n } from 'react-redux-i18n';
import arrayMutators from 'final-form-arrays';
import { type ApolloClient, compose, withApollo } from 'react-apollo';
import { Field } from 'react-final-form';
import { Table } from 'react-bootstrap';
import { Col, Form, FormGroup, FormControl, Pager } from 'react-bootstrap'

import CheckboxFieldAdapter from '../../form/checkboxFieldAdapter';
import SelectFieldAdapter from '../../form/selectFieldAdapter';
import { compareEditorState } from '../../form/utils';
import { decodeUserIdBase64 } from '../../../utils/globalFunctions';
import AdminForm from '../../../components/form/adminForm';
import LoadSaveReinitializeForm from '../../../components/form/LoadSaveReinitializeForm';
import { load, lazyLoad, postLoadFormat } from './load';

import { createMutationsPromises, save } from './save';
import validate from './validate';
import Loader from '../../common/loader';

type Props = {
  client: ApolloClient,
  editLocale: string,
  lang: string
};

const loading = <Loader />;

class PermissionsUsersForm extends React.Component<Props> {
  state = {
    typeAheadValue: "",
    lazyLoadValues: {
      adaptedProfiles: []
    },
    offset: 0,
    limit: 5
  }

  constructor(props) {
    super(props);
    this.loadSaveRenitForm = React.createRef();
    this.valuesRef = React.createRef();
  }

  render() {
    const { client } = this.props;
    const { lazyLoadValues, typeAheadValue, offset, limit } = this.state;

    return (
      <LoadSaveReinitializeForm
        ref={this.loadSaveRenitForm}
        load={(fetchPolicy: FetchPolicy) => load(client, fetchPolicy, offset, limit)}
        loading={loading}
        postLoadFormat={postLoadFormat}
        createMutationsPromises={createMutationsPromises(client)}
        save={save}
        validate={validate}
        localState={lazyLoadValues}
        getCurrentState={this.getCurrentState}
        mutators={{
          ...arrayMutators
        }}
        render={({ handleSubmit, submitting, values, initialValues }) => {
          // Don't use final form pristine here, we need special comparison for richtext fields
          const pristine = isEqualWith(initialValues, values, compareEditorState);
          
          return (
            <div className="admin-content">
              <Form horizontal>
                <FormGroup>
                  <FormControl
                    onChange={async (event) => {
                      this.setState({
                        typeAheadValue: event.target.value
                      })

                      if (event.target.value !== "") {
                        const originalValues = await lazyLoad(client, 'cache-first', event.target.value, offset, limit)
                        const lazyLoadValues = postLoadFormat ? postLoadFormat(originalValues) : (originalValues: any);
                        if (lazyLoadValues) {
                          this.setState({
                            lazyLoadValues
                          })
                        } 
                      } else {
                        this.setState({
                          lazyLoadValues: {
                            adaptedProfiles: []
                          }
                        })
                      }
                    }}
                    type="email" 
                    validate={this.email}
                    name='email'
                    value={typeAheadValue}
                    placeholder={I18n.t('administration.permissions.emailexample')}
                  />
                </FormGroup>
                <FormGroup>
                  <Col xs={6}>
                    <div className="annotation padding-left">{I18n.t('administration.permissions.limit')}</div>
                  </Col>
                  <Col xs={4}>
                    <FormControl
                      componentClass="select"
                      value={limit}
                      onChange={async (event) => {
                        const node = this.loadSaveRenitForm.current;
                        const newLimit = parseInt(event.target.value, 10);

                        this.setState({
                          limit: newLimit
                        }, async () => {
                          await node.load('network-only')
                        })
                      }}
                    >
                      <option value="5">5</option>
                      <option value="10">10</option>
                      <option value="25">25</option>
                      <option value="50">50</option>
                    </FormControl>
                  </Col>
                </FormGroup>
              </Form>
              <AdminForm handleSubmit={handleSubmit} pristine={pristine} submitting={submitting}>
                <div>
                  <Table striped bordered hover responsive>
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>User</th>
                        <th>r:participant</th>
                        <th>r:administrator</th>
                        <th>r:catcher</th>
                        <th>r:moderator</th>
                        <th>r:sysadmin</th>
                      </tr>
                    </thead>
                    <tbody>
                      {values.adaptedProfiles && values.adaptedProfiles.map((profile, idx) => (
                      <tr key={idx}>
                        <td>{decodeUserIdBase64(profile.id)}</td>
                        <td>{`${profile.displayName} (${profile.email})`}<br/><span>{`Verified:${profile.verified}`}</span>&nbsp;<span>{`Login Failures: ${profile.loginFailures}`}</span></td>
                        <td>
                        <Field
                            component={CheckboxFieldAdapter}
                            name={`${idx}-r:participant`}
                            isChecked={values[`${idx}-r:participant`]}
                            type="checkbox"
                          />
                        </td>
                        <td>
                          <Field
                            component={CheckboxFieldAdapter}
                            name={`${idx}-r:administrator`}
                            isChecked={values[`${idx}-r:administrator`]}
                            type="checkbox"
                          />  
                        </td>
                        <td>
                          <Field
                            component={CheckboxFieldAdapter}
                            name={`${idx}-r:catcher`}
                            isChecked={values[`${idx}-r:catcher`]}
                            type="checkbox"
                          />
                        </td>
                        <td>
                          <Field
                            component={CheckboxFieldAdapter}
                            name={`${idx}-r:moderator`}
                            isChecked={values[`${idx}-r:moderator`]}
                            type="checkbox"
                          />
                        </td>
                        <td>
                          <Field
                            component={CheckboxFieldAdapter}
                            name={`${idx}-r:sysadmin`}
                            isChecked={values[`${idx}-r:sysadmin`]}
                            type="checkbox"
                          />
                        </td>
                      </tr>
                      ))}
                    </tbody>
                  </Table>
                  <Pager>
                    {offset > 0 ? 
                    (
                      <Pager.Item onClick={async ()=> {
                        const node = this.loadSaveRenitForm.current;
                        const newOffset = offset - 5;
                        this.setState({
                          offset: newOffset
                        }, async () => {
                          await node.load('cache-first', newOffset, limit)
                        })
                      }}>{I18n.t('search.pagination.previous')}</Pager.Item>
                    )
                    : null}
                    {values.more ? (
                    <Pager.Item onClick={async () => {
                      const node = this.loadSaveRenitForm.current;
                      const newOffset = offset + 5;
                      this.setState({
                        offset: newOffset
                      }, async () => {
                        await node.load('cache-first', newOffset, limit)
                      })
                    }}>{I18n.t('search.pagination.next')}</Pager.Item>) : null}
                  </Pager>
                </div>
              </AdminForm>
            </div>
          );
        }}
      />
    );
  }
}

const mapStateToProps = ({ admin: { editLocale }, i18n }) => ({
  editLocale: editLocale,
  lang: i18n.locale
});

export default compose(connect(mapStateToProps), withApollo)(PermissionsUsersForm);