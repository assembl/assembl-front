// @flow
import type { FileValue, I18nValue, I18nRichTextValue } from '../../form/types.flow';

export type AgentData = {
  agentProfiles: List
};
