// @flow
import React, { useEffect, useState } from "react";
import { I18n, Translate } from 'react-redux-i18n';
import moment from 'moment';
import classnames from 'classnames';
import { Link } from 'react-router';
import { FormGroup, Radio, Checkbox, FormControl } from 'react-bootstrap';
import { connect } from 'react-redux';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

import SectionTitle from './sectionTitle';
import { displayAlert } from '../../utils/utilityManager';
import { exportFirstVisitRequest, exportDoneRequest, exportLastVisitRequest } from '../../actions/exportActions';

const exportToCSV = (csvData, fileName) => {
  const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
  const fileExtension = '.xlsx';
  const ws = XLSX.utils.aoa_to_sheet(csvData);
  const wb = { Sheets: { 'data': ws }, SheetNames: ['data'] };
  const excelBuffer = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });
  const data = new Blob([excelBuffer], {type: fileType});
  FileSaver.saveAs(data, fileName + fileExtension);
}

const getDateString = () => {
  const date = new Date();
  const year = date.getFullYear();
  const month = `${date.getMonth() + 1}`.padStart(2, '0');
  const day =`${date.getDate()}`.padStart(2, '0');
  return `${year}${month}${day}`
}

function ExportVisits(props)  {
  const { 
    annotation, 
    buttonIsDisabled, 
    sectionTitle, 
    discussion: { discussionId },
    export: { visitData },
    disableExportButton,
    exportFirstVisitData,
    exportLastVisitData,
    exportDone
  } = props;

  const [firstVisit, setFirstVisit] = useState(true);
  const [lastVisit, setLastVisit] = useState(false);

  const exportButtonClassNames = classnames('button-link button-dark margin-l', { disabled: buttonIsDisabled });

  if (visitData.length > 0) {
    exportToCSV(visitData, `${getDateString()}-visitData`)
    exportDone();
  }

  return (
    <div className="admin-box admin-export-section">
      <SectionTitle
        title={I18n.t(`administration.export.${sectionTitle}`)}
        annotation={I18n.t(`administration.export.${annotation}`)}
      />
      <div className="admin-content">
        <FormGroup>
          <div>
            <Radio checked={firstVisit} onChange={() => {
              setFirstVisit(true);
              setLastVisit(false);
            }}>
              <Translate value="administration.export.dateoffirstvisit" />
            </Radio>
            <Radio checked={lastVisit} onChange={() => {
              setFirstVisit(false);
              setLastVisit(true);
            }}>
              <Translate value="administration.export.dateoflastvisit" />
            </Radio>
          </div>
        </FormGroup>
        <div className="center-flex">
          <Link className={exportButtonClassNames} href="#" onClick={(e) => {
              e.preventDefault();
              disableExportButton();
            
              if (firstVisit) {
                exportFirstVisitData(discussionId);
              } else {
                exportLastVisitData(discussionId);
              }
          }}>
            <Translate value="administration.export.link" />
          </Link>
        </div>
      </div>
    </div>
  );
};

const mapDispatchToProps = dispatch => ({
  exportFirstVisitData: (discussionnId) => {
    dispatch(exportFirstVisitRequest(discussionnId));
  },
  exportLastVisitData: (discussionnId) => {
    dispatch(exportLastVisitRequest(discussionnId));
  },
  exportDone: () => {
    dispatch(exportDoneRequest());
  },
});

const mapStateToProps = (state) => ({
  discussion: state.discussion,
  export: state.export
});

export default connect(mapStateToProps, mapDispatchToProps)(ExportVisits);