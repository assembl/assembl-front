// @flow
import type { FileValue } from '../../../form/types.flow';

export type PersonalizeInterfaceValues = {
  title: ?string,
  favicon: FileValue,
  logo: FileValue,
  firstColor: string,
  secondColor: string,
  webAnalyticsPiwikIdSite: integer,
  socialLogin: string,
  facebookActivated: Boolean,
  instagramActivated: Boolean,
  googleActivated: Boolean,
  twitterActivated: Boolean,
  linkedinActivated: Boolean,
  extraJson: String
};