// @flow
import type { ApolloClient } from 'react-apollo';
import isEmpty from 'lodash/isEmpty';

import type { PersonalizeInterfaceValues } from './types.flow';
import updateDiscussionPreference from '../../../../graphql/mutations/updateDiscussionPreference.graphql';
import { getFileVariable, createSave } from '../../../form/utils';

function getVariables(values, initialValues) {
  const initialFavicon = initialValues ? initialValues.favicon : null;
  const initialLogo = initialValues ? initialValues.logo : null;

  const variables = {
    tabTitle: values.title,
    favicon: getFileVariable(values.favicon, initialFavicon),
    logo: getFileVariable(values.logo, initialLogo),
    firstColor: values.firstColor,
    secondColor: values.secondColor,
    webAnalyticsPiwikIdSite: values.webAnalyticsPiwikIdSite === '' ? null : values.webAnalyticsPiwikIdSite,
    facebookActivated: values.facebookActivated && values.facebookActivated !== "" ? true : false,
    instagramActivated: values.instagramActivated && values.instagramActivated !== "" ? true : false,
    googleActivated: values.googleActivated && values.googleActivated !== "" ? true : false,
    twitterActivated: values.twitterActivated && values.twitterActivated !== "" ? true : false,
    linkedinActivated: values.linkedinActivated && values.linkedinActivated !== "" ? true : false,
    extraJson: isEmpty(JSON.parse(values.extraJson)) ? null : values.extraJson
  };

  return variables
}



export const createMutationsPromises = (client: ApolloClient) => (
  values: PersonalizeInterfaceValues,
  initialValues: PersonalizeInterfaceValues
) => {
  const updateMutation = () =>
    client.mutate({
      mutation: updateDiscussionPreference,
      variables: getVariables(values, initialValues)
    });
  return [updateMutation];
};

export const save = createSave('administration.personalizeInterface.success');