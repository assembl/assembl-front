// @flow
import React from 'react';
import { type ApolloClient, withApollo } from 'react-apollo';
import { I18n, Translate } from 'react-redux-i18n';
import { Field } from 'react-final-form';
import classNames from 'classnames';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFacebook, faGooglePlus, faLinkedin, faInstagram, faTwitter } from '@fortawesome/free-brands-svg-icons'
import LoadSaveReinitializeForm from '../../../form/LoadSaveReinitializeForm';
import SaveButton from '../../saveButton';
import SectionTitle from '../../sectionTitle';
import CheckboxFieldAdapter from '../../../../components/form/checkboxFieldAdapter';
import FileUploaderFieldAdapter from '../../../form/fileUploaderFieldAdapter';
import TextFieldAdapter from '../../../form/textFieldAdapter';
import { load, postLoadFormat } from './load';
import { createMutationsPromises, save } from './save';
import validate from './validate';
import Loader from '../../../common/loader';
import { DEFAULT_FAVICON, IMG_FAVICON_TITLE } from '../../../../constants';
import ColorPickerFieldAdapter from '../../../form/colorPickerFieldAdapter';

type Props = {
  client: ApolloClient
};

const loading = <Loader />;

const PersonalizeInterface = ({ client }: Props) => (
  <div className="admin-box">
    <SectionTitle title={I18n.t('administration.discussion.6')} annotation={I18n.t('administration.annotation')} />
    <div className="admin-language-content">
      <LoadSaveReinitializeForm
        load={(fetchPolicy: FetchPolicy) => load(client, fetchPolicy)}
        loading={loading}
        postLoadFormat={postLoadFormat}
        createMutationsPromises={createMutationsPromises(client)}
        save={save}
        afterSave={(values) => {
          // Update the title and the favicon of the page
          const head = document.head;
          if (head) {
            // Update the title
            const pageTitle = head.getElementsByTagName('title')[0];
            if (pageTitle) pageTitle.text = values.title || '';
            // Update the favicon
            const faviconLink = head.querySelector('link[rel="icon"]');
            // Use the default favicon if favicon is null
            // $FlowFixMe Cannot get `values.favicon.externalUrl` because property `externalUrl` is missing in `String`
            let faviconUrl = values.favicon ? values.favicon.externalUrl : DEFAULT_FAVICON;
            faviconUrl = typeof faviconUrl === 'object' ? window.URL.createObjectURL(faviconUrl) : faviconUrl;
            if (faviconLink) faviconLink.setAttribute('href', faviconUrl);
          }
        }}
        validate={validate}
        render={({ handleSubmit, pristine, submitting, values }) => (
          <React.Fragment>
            <div>
              <Translate value="administration.personalizeInterface.titleFormTitle" />
            </div>
            <div className="img-helper-container">
              <img className="img-helper" src={IMG_FAVICON_TITLE} alt="personalize-interface-helper" />
            </div>
            <form className="language-list" onSubmit={handleSubmit}>
              <SaveButton disabled={pristine || submitting} saveAction={handleSubmit} />
              <Field
                required
                name="title"
                component={TextFieldAdapter}
                label={I18n.t('administration.personalizeInterface.title')}
              />
              <div className="separator" />
              <Field
                name="favicon"
                component={FileUploaderFieldAdapter}
                label={I18n.t('administration.personalizeInterface.favicon')}
              />
              <p className="label-indication">{I18n.t('administration.personalizeInterface.faviconInstruction')}</p>
              <div className="separator" />
              <Field
                component={FileUploaderFieldAdapter}
                name="logo"
                label={I18n.t('administration.discussionPreferences.debateLogoLabel')}
              />
              <p className="label-indication">{I18n.t('administration.personalizeInterface.logoInstruction')}</p>
              <div className="separator" />
              <div className="title">
                <Translate value="administration.theme.themeColorsTitle" />
              </div>
              <Field component={ColorPickerFieldAdapter} name="firstColor" label={I18n.t('administration.theme.firstColor')} />
              <Field component={ColorPickerFieldAdapter} name="secondColor" label={I18n.t('administration.theme.secondColor')} />
              <div className="separator" />
              <div className="title">
                <Translate value="administration.matomoIDTitle" />
              </div>
              <Field
                component={TextFieldAdapter}
                name="webAnalyticsPiwikIdSite"
                label={I18n.t('administration.matomoID')}
              />
              <div className="separator" />
              <div className="title">
                <Translate value="administration.socialLogins.socialLoginsTitle" />
              </div>
              <Field
                component={CheckboxFieldAdapter}
                name="facebookActivated"
                isChecked={values.facebook}
                label={I18n.t('administration.socialLogins.activateFacebook')}
                type="checkbox"
                icon={<FontAwesomeIcon icon={faFacebook} size="lg" />}
              />
              <Field
                component={CheckboxFieldAdapter}
                name="instagramActivated"
                isChecked={values.instagram}
                label={I18n.t('administration.socialLogins.activateInstagram')}
                type="checkbox"
                icon={<FontAwesomeIcon icon={faInstagram} size="lg" />}
              />
              <Field
                component={CheckboxFieldAdapter}
                name="googleActivated"
                isChecked={values.google}
                label={I18n.t('administration.socialLogins.activateGoogle')}
                type="checkbox"
                icon={<FontAwesomeIcon icon={faGooglePlus} size="lg" />}
              />
              <Field
                component={CheckboxFieldAdapter}
                name="twitterActivated"
                isChecked={values.twitter}
                label={I18n.t('administration.socialLogins.activateTwitter')}
                type="checkbox"
                icon={<FontAwesomeIcon icon={faTwitter} size="lg" />}
              />
              <Field
                component={CheckboxFieldAdapter}
                name="linkedinActivated"
                isChecked={values.linkedin}
                label={I18n.t('administration.socialLogins.activateLinkedIn')}
                type="checkbox"
                icon={<FontAwesomeIcon icon={faLinkedin} size="lg" />}
              />
              <div className="separator" />
              <div className="title">
                <Translate value="administration.extraJsonTitle" />
              </div>
              <Field
                component={TextFieldAdapter}
                name="extraJson"
                as="textarea"
                label={I18n.t('administration.extraJson')}
              />
            </form>
          </React.Fragment>
        )}
      />
    </div>
  </div>
);

export default withApollo(PersonalizeInterface);