// @flow
import { I18n } from 'react-redux-i18n';
import type { DiscussionPreferencesFormValues } from './types.flow';

type Errors = {
    externalHomepageUrl?: string
};

type Values = {
    externalHomepageUrl: ?string
} & DiscussionPreferencesFormValues;

const checkHomePageURL = (homepageURL: ?string) => {
    if (!homepageURL) {
        return 
    }
    const urlRegex = /^((http(s?)?):\/\/)?([wW]{3}\.)?[a-zA-Z0-9\-.]+\.[a-zA-Z]{2,}(\.[a-zA-Z]{2,})?$/g;
    const result = homepageURL.match(urlRegex);
    if (result === null) {
        return I18n.t('administration.externalhomepage.error');
    }
};

export default function validate(values: Values): Errors {
    const errors = {};
  
    if (values.externalHomepageUrl && values.externalHomepageUrl !== "") {
      const homePageURLErrors = checkHomePageURL(values.externalHomepageUrl);

      if (homePageURLErrors) {
        errors.externalHomepageUrl = homePageURLErrors;
      }  
    }
  
    return errors;
}

