// @flow
import { type CheckboxListValue } from '../../../form/types.flow';

export type DiscussionPreferencesFormValues = {
  languages: CheckboxListValue,
  withModeration: boolean,
  withTranslation: Boolean,
  withSemanticAnalysis: Boolean,
  slug: string,
  webAnalyticsPiwikIdSite: integer,
  facebookActivated: Boolean,
  instagramActivated: Boolean,
  googleActivated: Boolean,
  twitterActivated: Boolean,
  linkedinActivated: Boolean,
  privateDebate: Boolean,
  genericErrors: Boolean,
  requireEmailDomain: Array,
  externalhomepage: Boolean,
  homepageURL: string
};