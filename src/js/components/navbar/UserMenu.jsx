// @flow
import * as React from 'react';
import { Link } from 'react-router';

import Search from '../search';
import Avatar from '../common/avatar';
import Permissions,{ connectedUserCan } from '../../utils/permissions';
import { getConnectedUserId } from '../../utils/globalFunctions';;
import HarvestingButton from './harvestingButton';

type UserMenuProps = {
  location: string,
  helpUrl: string,
  loginData: ?Object
};

const UserMenu = ({ location, helpUrl, loginData }: UserMenuProps) => (
  <div className="navbar-icons">
    {connectedUserCan(Permissions.ADD_EXTRACT) && <HarvestingButton />}
    <div id="search">
      <Search />
    </div>
    {getConnectedUserId() &&
      helpUrl && (
        <Link to={helpUrl} target="_blank" rel="noopener noreferrer">
          <span className="assembl-icon-faq grey" />
        </Link>
      )}
    <Avatar location={location} loginData={loginData} split />
  </div>
);

UserMenu.defaultProps = {
  remainingWidth: null
};

export default UserMenu;