// @flow
import React, { lazy } from 'react';
import { Route, Redirect } from 'react-router';
import Root from './root';
import ApplicationContainer from './appContainer';
import { routeForRouter } from './utils/routeMap';

const DebateHome = (props) => {
  return <DebateThread {...props} />;
};

const DebateChild = (props) => {
  return <Idea id={props.id} identifier={props.identifier} phaseId={props.phaseId} routerParams={props.params} />;
};

// //  Lazy loading

const App = lazy(() => import(/* webpackChunkName: "app" */ './app'));
const Main = lazy(() => import(/* webpackChunkName: "main" */ './main'));

const Login = lazy(() => import(/* webpackChunkName: "login" */ './pages/login'));
const Signup = lazy(() => import(/* webpackChunkName: "signup" */ './pages/signup'));
const ChangePassword = lazy(() => import(/* webpackChunkName: "ChangePassword" */ './pages/changePassword'));
const RequestPasswordChange = lazy(() => import(/* webpackChunkName: "RequestPasswordChange" */ './pages/requestPasswordChange'));
const Home = lazy(() => import(/* webpackChunkName: "main" */ './pages/home'));
const Syntheses = lazy(() => import(/* webpackChunkName: "syntheses" */ './pages/syntheses'));
const Synthesis = lazy(() => import(/* webpackChunkName: "synthesis" */ './pages/synthesis'));
const DebateThread = lazy(() => import(/* webpackChunkName: "debateThread" */ './pages/debateThread'));
const Idea = lazy(() => import(/* webpackChunkName: "idea" */ './pages/idea'));
const Community = lazy(() => import(/* webpackChunkName: "community" */ './pages/community'));
const Question = lazy(() => import(/* webpackChunkName: "question" */ './pages/question'));
const QuestionModeratePosts = lazy(() => import(/* webpackChunkName: "questionModeratePosts" */ './pages/questionModeratePosts'));
const Profile = lazy(() => import(/* webpackChunkName: "profile" */ './pages/profile'));
const NotFound = lazy(() => import(/* webpackChunkName: "commnotFoundunity" */ './pages/notFound'));
const TermsAndConditions = lazy(() => import(/* webpackChunkName: "termsAndConditions" */ './pages/termsAndConditions'));
const LegalNotice = lazy(() => import(/* webpackChunkName: "legalNotice" */ './pages/legalNotice'));
const PrivacyPolicy = lazy(() => import(/* webpackChunkName: "privacyPolicy" */ './pages/privacyPolicy'));
const CookiesPolicy = lazy(() => import(/* webpackChunkName: "cookiesPolicy" */ './pages/cookiesPolicy'));
const UserGuidelines = lazy(() => import(/* webpackChunkName: "userGuidelines" */ './pages/userGuidelines'));
const Administration = lazy(() => import(/* webpackChunkName: "administration" */ './pages/administration'));
// eslint-disable-next-line max-len
const UnauthorizedAdministration = lazy(() => import(/* webpackChunkName: "unauthorizedAdministration" */ './pages/unauthorizedAdministration'));
const ResourcesCenterAdmin = lazy(() => import(/* webpackChunkName: "resourcesCenterAdmin" */ './pages/resourcesCenterAdmin'));
const PermissionsAdmin = lazy(() => import(/* webpackChunkName: "peeremissionsAdmin" */ './pages/permissionsAdmin'));
const SurveyAdmin = lazy(() => import(/* webpackChunkName: "surveyAdmin" */ './pages/surveyAdmin'));
const DiscussionAdmin = lazy(() => import(/* webpackChunkName: "discussionAdmin" */ './pages/discussionAdmin'));
const VoteSessionAdmin = lazy(() => import(/* webpackChunkName: "voteSessionAdmin" */ './pages/voteSessionAdmin'));
const ResourcesCenter = lazy(() => import(/* webpackChunkName: "resourcesCenter" */ './pages/resourcesCenter'));
const SemanticAnalysisForDiscussion = lazy(
  () => import(/* webpackChunkName: "semanticAnalysisForDiscussion" */
    './pages/semanticAnalysis/semanticAnalysisForDiscussion/semanticAnalysisForDiscussion'
  )
);
const LandingPageAdmin = lazy(() => import(/* webpackChunkName: "landingPageAdmin" */ './pages/landingPageAdmin'));
const BrightMirrorFiction = lazy(() => import(/* webpackChunkName: "brightMirrorFiction" */ './pages/brightMirrorFiction'));
const ExportData = lazy(() => import(/* webpackChunkName: "exportData" */ './pages/exportData'));

const CreateSynthesisForm = lazy(
  () => import(/* webpackChunkName: "synthesisIndex" */ './components/administration/synthesis/index')
);


const AdminChild = (props: {
  discussionPhaseId: string,
  location: { query: { section?: string, thematicId?: string, goBackPhaseIdentifier?: string } },
  params: { phase: string }
}) => {
  switch (props.params.phase) {
  case 'discussion':
    return <DiscussionAdmin {...props} section={props.location.query.section} />;
  case 'voteSession':
    return (
      <VoteSessionAdmin
        {...props}
        phaseIdentifier={props.params.phase}
        goBackPhaseIdentifier={props.location.query.goBackPhaseIdentifier}
        thematicId={props.location.query.thematicId}
        section={props.location.query.section}
      />
    );
  case 'resourcesCenter':
    return <ResourcesCenterAdmin {...props} />;
  case 'permissions':
    return <PermissionsAdmin {...props} />;
  case 'landingPage':
    return <LandingPageAdmin {...props} section={props.location.query.section} />;

  case 'exportDebateData':
    return <ExportData {...props} section={props.location.query.section} />;
  default:
    return (
      <SurveyAdmin
        {...props}
        phaseIdentifier={props.params.phase}
        thematicId={props.location.query.thematicId}
        section={props.location.query.section}
      />
    );
  }
};

const BuildBrightMirrorFiction = props => <BrightMirrorFiction {...props.params} phaseId={props.phaseId} />;

export default [
  <Route path="/" component={ApplicationContainer}>
    <Route component={Root}>
      {/* Those login routes should be kept in synchrony with assembl.views.auth.__init__.py */}
      <Route path={routeForRouter('login', true)} component={Login} />
      <Route path={routeForRouter('signup', true)} component={Signup} />
      <Route path={routeForRouter('changePassword', true)} component={ChangePassword} />
      <Route path={routeForRouter('requestPasswordChange', true)} component={RequestPasswordChange} />

      {/* These are contextual routes for the ones above */}
      {/* TODO: eventually refactor Main into App */}
      <Route component={App}>
        <Route component={Main}>
          <React.Fragment>
            <Redirect from={routeForRouter('homeBare')} to={routeForRouter('home')} />
            <Route path={routeForRouter('home')} component={Home} />
            <Route path={routeForRouter('homeBare')} component={Home} />
          </React.Fragment>
          <Route path={routeForRouter('profile', false, { userId: ':userId' })} component={Profile} />
          <Route path={routeForRouter('syntheses')} component={Syntheses} />
          <Route path={routeForRouter('createSynthesis')} components={CreateSynthesisForm} />
          <Route
            path={routeForRouter('editSynthesis', false, { synthesisId: ':synthesisId' })}
            components={CreateSynthesisForm}
          />
          <Route path={routeForRouter('synthesis', false, { synthesisId: ':synthesisId' })} component={Synthesis} />
          <Route path={routeForRouter('resourcesCenter')} component={ResourcesCenter} />
          <Route path={routeForRouter('semanticAnalysis')} component={SemanticAnalysisForDiscussion} />
          <Route path={routeForRouter('legalNotice')} component={LegalNotice} />
          <Route path={routeForRouter('privacyPolicy')} component={PrivacyPolicy} />
          <Route path={routeForRouter('cookiesPolicy')} component={CookiesPolicy} />
          <Route path={routeForRouter('userGuidelines')} component={UserGuidelines} />
          <Route path={routeForRouter('terms')} component={TermsAndConditions} />
          <Route path={routeForRouter('community')} component={Community} />
          <Route path={routeForRouter('rootDebate')} />
          <Route path={routeForRouter('debate', false, { phase: ':phase' })} component={DebateHome}>
            <Route path={routeForRouter('theme', false, { themeId: ':themeId' })} component={DebateChild} />
            <Route
              path={routeForRouter('question', false, {
                questionId: ':questionId',
                questionIndex: ':questionIndex'
              })}
              component={Question}
            />
            <Route
              path={routeForRouter('questionModeratePosts', false, {
                questionId: ':questionId',
                questionIndex: ':questionIndex'
              })}
              component={QuestionModeratePosts}
            />
          </Route>
          <Route
            path={routeForRouter('brightMirrorFiction', false, { phase: ':phase', themeId: ':themeId', fictionId: ':fictionId' })}
            component={BuildBrightMirrorFiction}
          />
          <Route path={routeForRouter('unauthorizedAdministration')} component={UnauthorizedAdministration} />
          <Route path={routeForRouter('administrationRoot')} component={Administration}>
            <Route path={routeForRouter('adminPhase', false, { phase: ':phase' })} component={AdminChild} />
          </Route>
        </Route>
      </Route>
    </Route>
  </Route>,
  <Route path="*" component={NotFound} />
];