import { xmlHttpRequest } from '../utils/httpRequestHandler';

/**
* Get the first visit data 
**/
export const exportFirstVisit = async (discussionId) => {
  const exportUrl = `/data/Discussion/${discussionId}/visitors?first=true`;
  const response = await xmlHttpRequest({ method: 'GET', url: exportUrl });

  return response;
};

/**
* Get the last visit data 
**/
export const exportLastVisit = (discussionId) => {
  const userUrl = `/data/Discussion/${discussionId}/visitors`;
  const userRequest = xmlHttpRequest({ method: 'GET', url: userUrl });

  return Promise.all([userRequest]).then((results) => {
    const data = results[0];
    return data
  });
};