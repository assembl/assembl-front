import { xmlHttpRequest } from '../utils/httpRequestHandler';
import { PasswordMismatchError } from '../utils/errors';
import { tokenCategoriesById } from '../reducers/adminReducer/voteSession';
import { filterGaugeVoteModules } from '../pages/voteSession';

export const postChangePassword = (payload) => {
  const route = '/data/AgentProfile/do_password_change';
  if (payload.password1 !== payload.password2) {
    return Promise.reject(new PasswordMismatchError('Passwords do not match!'));
  }

  return xmlHttpRequest({
    method: 'POST',
    url: route,
    isJson: true,
    payload: payload
  });
};

export const signUp = async (payload) => {
  const {
    privacyPolicyIsChecked,
    termsAndConditionsIsChecked,
    userGuidelinesIsChecked,
    captchaIsChecked,
    discussionSlug,
    email,
    fullname,
    password,
    password2,
    username,
    captchaToken,
    ...rest
  } = payload;

  if (password !== password2) {
    return Promise.reject(new PasswordMismatchError('Passwords do not match!'));
  }

  let route;
  if (!discussionSlug) {
    route = '/data/User';
  } else {
    route = `/data/Discussion/${discussionSlug}/all_users`;
  }

  const newPayload = {
    username: username || null,
    real_name: fullname,
    password: password,
    accounts: [
      {
        email: email,
        '@type': 'EmailAccount'
      }
    ],
    captchaToken: captchaToken,
    profileFields: rest
  };

  return xmlHttpRequest({
    method: 'POST',
    url: route,
    isJson: true,
    payload: newPayload
  });
};

export const signIn = async (payload, discussionSlug) => {
  const route = `/debate/${discussionSlug}/login`;

  return xmlHttpRequest({
    method: 'POST',
    url: route,
    payload: payload
  });
};


export const changePasswordRequest = (id, discussionSlug) => {
  const route = '/data/AgentProfile/password_reset';
  const payload = {
    identifier: id
  };

  if (discussionSlug) {
    payload.discussion_slug = discussionSlug;
  }

  return xmlHttpRequest({
    method: 'POST',
    url: route,
    isJson: true,
    payload: payload
  });
};