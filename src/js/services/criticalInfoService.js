import { xmlHttpRequest } from '../utils/httpRequestHandler';

/**
 * This function will use the cookie session server side to return either
 * the user info or null if not connecte
 **/
export const getCriticalInfo = async (discussionId) => {
  const userUrl = `/critical_information?discussion_id=${discussionId}`;
  return await xmlHttpRequest({ method: 'GET', url: userUrl });
};