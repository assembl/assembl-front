// @flow
import React from 'react';
import { I18n } from 'react-redux-i18n';

import SectionTitle from '../components/administration/sectionTitle';
import PermissionsUsersForm from '../components/administration/permissions';

const PermissionsAdmin = () => (
  <div className="resources-center-admin admin-box admin-content">
    <SectionTitle title={I18n.t('administration.permissions.title')} annotation={I18n.t('administration.permissions.description')} />
    <PermissionsUsersForm />
  </div>
);

export default PermissionsAdmin;