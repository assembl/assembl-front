// @flow
import React from 'react';
import { compose, graphql } from 'react-apollo';
import moment from 'moment';
import { connect } from 'react-redux';
import ExportSection from '../components/administration/exportSection';
import ExportVisits from '../components/administration/exportVisits';
import manageErrorAndLoading from '../components/common/manageErrorAndLoading';

import DiscussionPreferences from '../graphql/DiscussionPreferences.graphql';
import ExportData from '../graphql/mutations/exportData.graphql';
import { PublicationStates, ExportDataTypes } from '../constants';
import type { ExportDataTypesType } from '../types.flow';

type Props = {
  section: string,
  locale: string,
  languages: Array<Language>,
  phases: Timeline,
  exportDataMutation: Function
};

type State = {
  shouldTranslate: boolean,
  exportLocale: string,
  shouldBeAnonymous: boolean,
  start: ?moment,
  end: ?moment,
  buttonIsDisabled: boolean
};

export class DumbExportData extends React.Component<Props, State> {
  state = {
    exportLocale: '',
    shouldTranslate: false,
    shouldBeAnonymous: false,
    start: null,
    end: null,
    buttonIsDisabled: false
  };

  handleDatesChange = ({ startDate, endDate }: DateRange) => {
    this.enableExportButton();
    this.setState({ start: startDate, end: endDate });
  };

  handleShouldTranslate = (shouldTranslate: boolean) => {
    this.enableExportButton();
    this.setState({ shouldTranslate: shouldTranslate });
  };

  toggleAnonymousOption = () => {
    this.enableExportButton();
    this.setState(prevState => ({ shouldBeAnonymous: !prevState.shouldBeAnonymous }));
  };

  handleExportLocaleChange = (exportLocale: string) => {
    this.enableExportButton();
    this.setState({
      exportLocale: exportLocale
    });
  };

  disableExportButton = () => {
    this.setState({ buttonIsDisabled: true });
  };

  enableExportButton = () => {
    this.setState({ buttonIsDisabled: false });
  };

  getMutationDataForType = (
    exportType: ExportDataTypesType,
    anonymous: ?boolean,
    startDate: ?string,
    endDate: ?string,
    locale: ?string
  ) => {
    const data: Object = {
      exportType: exportType,
      anonymous: anonymous
    };
    if (exportType === ExportDataTypes.MULTI_MODULE_POSTS) {
      data.publicationState = [
        PublicationStates.DELETED_BY_ADMIN,
        PublicationStates.MODERATED_TEXT_NEVER_AVAILABLE,
        PublicationStates.MODERATED_TEXT_ON_DEMAND
      ];
    }
    if (startDate) {
      data.start = startDate;
    }
    if (endDate) {
      data.end = endDate;
    }
    if (locale) {
      data.locale = locale;
    }
    return data;
  };

  render() {
    const { section, languages, phases, exportDataMutation } = this.props;
    const { exportLocale, shouldBeAnonymous, shouldTranslate, start, end, buttonIsDisabled } = this.state;
    const locale = exportLocale || (languages && this.props.locale);
    const translation = shouldTranslate && locale ? locale : null;
    const anonymous = shouldBeAnonymous;
    const startDate = start ? start.toISOString() : null;
    const endDate = end ? end.toISOString() : null;

    return (
      <div>
        {section === '1' && (
          <ExportSection
            annotation="contributions"
            sectionTitle="contributions"
            handleDatesChange={this.handleDatesChange}
            handleAnonymousChange={this.toggleAnonymousOption}
            handleShouldTranslate={this.handleShouldTranslate}
            handleExportLocaleChange={this.handleExportLocaleChange}
            locale={locale}
            shouldBeAnonymous={shouldBeAnonymous}
            shouldTranslate={shouldTranslate}
            exportLocale={exportLocale}
            exportDataMutation={exportDataMutation}
            customMutationProps={this.getMutationDataForType(
              ExportDataTypes.MULTI_MODULE,
              anonymous,
              startDate,
              endDate,
              translation
            )}
            phases={phases}
            start={start}
            end={end}
            languages={languages}
            disableExportButton={this.disableExportButton}
            buttonIsDisabled={buttonIsDisabled}
          />
        )}
        {section === '2' && (
          <ExportSection
            sectionTitle="taxonomySectionTitle"
            annotation="taxonomyAnnotation"
            exportDataMutation={exportDataMutation}
            customMutationProps={this.getMutationDataForType(
              ExportDataTypes.TAXONOMY,
              anonymous,
              startDate,
              endDate,
              translation
            )}
            disableExportButton={this.disableExportButton}
            buttonIsDisabled={buttonIsDisabled}
          />
        )}
        {section === '3' && (
          <ExportSection
            sectionTitle="usersSectionTitle"
            annotation="usersAnnotation"
            handleDatesChange={this.handleDatesChange}
            handleAnonymousChange={this.toggleAnonymousOption}
            locale={locale}
            shouldBeAnonymous={shouldBeAnonymous}
            shouldTranslate={shouldTranslate}
            exportDataMutation={exportDataMutation}
            customMutationProps={this.getMutationDataForType(ExportDataTypes.USERS, anonymous, startDate, endDate, translation)}
            phases={phases}
            start={start}
            end={end}
            languages={languages}
            disableExportButton={this.disableExportButton}
            buttonIsDisabled={buttonIsDisabled}
          />
        )}
        {section === '4' && (
          <ExportSection
            annotation="moderatedContributionsAnnotation"
            sectionTitle="moderatedContributions"
            handleDatesChange={this.handleDatesChange}
            handleAnonymousChange={this.toggleAnonymousOption}
            handleShouldTranslate={this.handleShouldTranslate}
            handleExportLocaleChange={this.handleExportLocaleChange}
            locale={locale}
            shouldBeAnonymous={shouldBeAnonymous}
            shouldTranslate={shouldTranslate}
            exportLocale={exportLocale}
            exportDataMutation={exportDataMutation}
            customMutationProps={this.getMutationDataForType(
              ExportDataTypes.MULTI_MODULE_POSTS,
              anonymous,
              startDate,
              endDate,
              translation
            )}
            phases={phases}
            start={start}
            end={end}
            languages={languages}
            disableExportButton={this.disableExportButton}
            buttonIsDisabled={buttonIsDisabled}
          />
        )}
        {section === '5' && (
          <ExportVisits 
            sectionTitle="visits" 
            annotation="visitsAnnotation" 
            buttonIsDisabled={buttonIsDisabled}
            disableExportButton={this.disableExportButton}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = ({ timeline, i18n }) => ({
  phases: timeline,
  locale: i18n.locale
});

export default compose(
  connect(mapStateToProps),
  graphql(ExportData, {
    name: 'exportDataMutation'
  }),
  graphql(DiscussionPreferences, {
    options: ({ locale }) => ({
      variables: {
        inLocale: locale
      }
    }),
    props: ({ data }) => {
      if (data.error || data.loading) {
        return {
          error: data.error,
          loading: data.loading
        };
      }

      return {
        error: data.error,
        loading: data.loading,
        languages: data.discussionPreferences.languages
      };
    }
  }),
  manageErrorAndLoading({ displayLoader: true })
)(DumbExportData);