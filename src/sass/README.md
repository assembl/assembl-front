# Overview

This directory contains all the sass files in a 7-1 pattern for the assembl front.

https://sass-guidelin.es/#the-7-1-pattern

# TODO

- [x] Move all .scss scattered around project to one area
- [ ] Convert all files to proper 7-1 distribution so it's easier to find and maintain
- [ ] Remove all unused classes from the application
