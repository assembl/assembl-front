export default {
    api: {
      graphql: process.env.API_GRAPHQL || 'https://sandbox-assembl.cloud.bluenove.com',
      search: process.env.API_SEARCH || 'https://sandbox-assembl.cloud.bluenove.com',
      api2: process.env.API_API2 || 'https://sandbox-assembl.cloud.bluenove.com',
      social: process.env.API_SOCIAL || 'https://sandbox-assembl.cloud.bluenove.com',
      root: process.env.API_ROOT || 'https://sandbox-assembl.cloud.bluenove.com',
    },
    build: {
        dir: process.env.BUILD_DIR || "build",
    },
    defaultSlug: process.env.DEFAULT_SLUG || 'qa-enrique',
    cookieDomain: process.env.COOKIE_DOMAIN || 'localhost',
    server: {
      host: process.env.SERVER_HOST || "localhost",
      port: parseInt(process.env.PORT, 10) || 3000
    },
};
  
